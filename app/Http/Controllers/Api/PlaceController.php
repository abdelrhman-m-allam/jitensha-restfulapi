<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Place;
use App\Presenters\Presenter;
use App\Presenters\PlacePresenter;
use App\Repositories\PlaceRepository;


class PlaceController extends Controller
{
	/**
	* @var App\Repositories\User\PlaceRepository
	*/
	protected $place;
	 

	/**
	* @var App\Presenters\Presenter
	*/
	protected $presenter;

	/**
	* @var App\Presenters\PlacePresenter
	*/
	protected $placePresenter;

    function __construct(PlaceRepository $place, Presenter $presenter, PlacePresenter $placePresenter){
    	$this->place = $place;
		$this->presenter = $presenter;
		$this->placePresenter = $placePresenter;
    }


    function index(){
    	$places = $this->place->all();
    	$places = $this->presenter->collection($places, new PlacePresenter);

    	return response()->json(compact('places'), 200);
    }
}
