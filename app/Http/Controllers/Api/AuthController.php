<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use JWTAuth;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\LoginRequest;

use App\Models\User;
use App\Services\UserService;

class AuthController extends Controller
{
    //

    protected $userService;
    function __construct(UserService $userService){
        $this->userService = $userService;
    }

    
    public function register(RegisterRequest $request){    	
    	// extract input
        $register_user = new User;
        $register_user->email = $request->email; 
        $register_user->password = $request->password; 

        // apply business logic
        $user = $this->userService->register($register_user);
        $token = JWTAuth::fromUser($user);

        // response
        return response()->json(['accessToken' =>  $token], Response::HTTP_CREATED);

    }


    public function login(LoginRequest $request){
    	
        // extract input
        $credentials = $request->only(['email', 'password']);

        // apply logic
        if (!$token = JWTAuth::attempt($credentials)){
            return response()->json(['errors' => ['credentials' => ['Invalid email or password'] ]],Response::HTTP_UNAUTHORIZED);
        }

        // response
        return response()->json(['accessToken' => $token],Response::HTTP_OK);
    	
    }
}
