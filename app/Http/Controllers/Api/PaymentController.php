<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \Illuminate\Http\Response;
use App\Http\Requests\RentRequest;

class PaymentController extends Controller
{
    /**
     * rent action
     * rent a something <>
     *
     * @param RentRequest $request
     * @return repsponse text/json
     *
     */
    public function rent(RentRequest $request){
    	return response()->json(["message" => "success"], Response::HTTP_OK);
    }
}
