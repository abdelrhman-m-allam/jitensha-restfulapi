<?php

namespace App\Services;

use App\Models\User;
use App\Repositories\UserRepository;
use Hash;

/**
* 
*/
class UserService
{

	protected $repository;

	function __construct(UserRepository $repository)
	{
		$this->repository= $repository;
	}

	function register(User $user){
		$new_user = new User;
		$new_user->email = $user->email;
		$new_user->password = bcrypt($user->password);

		return $this->repository->add($new_user);
	}
}