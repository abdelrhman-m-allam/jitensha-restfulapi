<?php 

namespace App\Presenters;
 
interface Presentable {
	public function present();
}