<?php


namespace App\Presenters;


use App\Models\Location;
use Illuminate\Support\Facades\Crypt;
/**
* Place Presenter 
* formate data and attributes
*/
class PlacePresenter extends AbstractPresenter implements Presentable {

	public function location()
	{
		$location = new Location;
		$location->lat = $this->object->lat;
		$location->lng = $this->object->lng;

		return $location;
	}

	public function name(){
		return $this->object->name;
	}


	public function id(){
		return Crypt::encryptString($this->object->id);
	}

	public function present(){
		$object = [];
		$object['id'] = $this->id();
		$object['name'] = $this->name();
		$object['location'] = $this->location();

		return $object;
	}
}