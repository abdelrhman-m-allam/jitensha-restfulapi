<?php

namespace App\Repositories;


use App\Models\Place;


/**
* Place Repository
*/
class PlaceRepository
{

	/**
	 * model injected to repository
	 *
	 */
	protected $model;


	function __construct(Place $place)
	{
		$this->model = $place;
	}

	function all(){
		return $this->model->all();
	}
}