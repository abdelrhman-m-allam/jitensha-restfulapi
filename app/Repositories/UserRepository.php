<?php

namespace App\Repositories;


use App\Models\User;

/**
* User Repository
*/
class UserRepository
{

	/**
	 * model injected to repository
	 *
	 */
	protected $model;


	function __construct(User $model)
	{
		$this->model = $model;
	}

	function add(User $user){
		$user->save();

		return $user;
	}
}