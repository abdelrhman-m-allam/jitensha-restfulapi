<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['prefix' => 'v1'], function(){

	// auth API
	Route::group(['prefix' => 'auth'], function(){
		Route::post('/register', 'Api\AuthController@register');
		Route::post('/login', 'Api\AuthController@login');
	});

	// authenticated header to access the following API: ref JWT/tymon
	Route::group(['middleware' => 'jwt.auth'], function(){
		Route::get('/places', 'Api\PlaceController@index');
		Route::post('/rent', 'Api\PaymentController@rent');
	});


});
