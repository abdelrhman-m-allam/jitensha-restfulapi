<?php

namespace Tests\Feature\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Models\User;

class RegisterTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testRequiresInputsJson()
    {

    	$this->json('POST', 'api/v1/auth/register')
    	->assertStatus(400)
         ->assertJson([
	                'email' => ['The email field is required.'],
	                'password' => ['The password field is required.'],
	            ]);
    }

    public function testUserRegisterSuccfully(){
    	$user = factory(User::class)->make(['password'=> 'secret']); 

    	$payload = ['email' => $user->email, 'password' => 'secret'];

    	$this->json('POST', 'api/v1/auth/register', $payload)
    	->assertStatus(201)
        ->assertJsonStructure([
            'accessToken',
        ]);
    }
}
