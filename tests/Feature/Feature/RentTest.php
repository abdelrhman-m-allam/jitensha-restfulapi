<?php

namespace Tests\Feature\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use JWTAuth;
use App\Models\User;
class RentTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testRentSuccefully()
    {    	

    	$user = User::all()->first();

        $token = JWTAuth::fromUser($user);

        $headers = ['Authorization' => "Bearer $token"];

        $card = \Faker\Factory::create();

        $payload = [];
        $payload['number'] = $card->creditCardNumber;
        $payload['name'] = $card->name;
        $payload['expiration'] = $card->creditCardExpirationDateString;
        $payload['code'] = '123';

   
        $this->json('POST', '/api/v1/rent', $payload, $headers)
        ->assertStatus(200)
        ->assertJsonStructure([
        	'message']);
    }
}
