<?php

namespace Tests\Feature\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

Use JWTAuth;
use App\Models\User;

class PlaceTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
    	$user = User::all()->first();

        $token = JWTAuth::fromUser($user);

        $headers = ['Authorization' => "Bearer $token"];

        $this->json('get', '/api/v1/places', [], $headers)
        ->assertStatus(200)
        ->assertJsonStructure([
        	'places'=>[[
        	 	"id", "name", "location"=> ["lat", "lng"]]
        	]]);
        
        
    }
}
