<?php

namespace Tests\Feature\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Models\User;

class LoginTest extends TestCase
{
	 public function testRequiresEmailAndLogin()
	    {
	        $this->json('POST', 'api/v1/auth/login')
	            ->assertStatus(400)
	            ->assertJson([
	                'email' => ['The email field is required.'],
	                'password' => ['The password field is required.'],
	            ]);
	    }


	    public function testUserLoginsSuccessfully()
	    {
	    	
	        $user = factory(User::class)->create(['password'=>bcrypt('secret')]); 

	        $payload = ['email' =>  $user->email, 'password' =>  'secret'];
	        

	        $this->json('POST', 'api/v1/auth/login', $payload)
	            ->assertStatus(200)
	            ->assertJsonStructure([
	                'accessToken',
	            ]);
	    }
}
