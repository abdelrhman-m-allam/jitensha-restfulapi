<?php

use Illuminate\Database\Seeder;
use App\Models\Place;

class PlacesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // Let's truncate our existing records to start from scratch.
        Place::truncate();

        $faker = \Faker\Factory::create('ja_JP');

        // And now, let's create a few articles in our database:
        for ($i = 0; $i < 50; $i++) {
            Place::create([
                'name' => $faker->address,
                'lat' => $faker->latitude,
                'lng' => $faker->longitude,
            ]);
        }
    }
}
