<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's clear the users table first
        User::truncate();

        $faker = \Faker\Factory::create();

        $password = Hash::make('allam');

        User::create([
            'email' => 'admin@email.com',
            'password' => $password,
        ]);

        
        for ($i = 0; $i < 10; $i++) {
            User::create([
                'email' => $faker->email,
                'password' => $password,
            ]);
        }
    }
}
